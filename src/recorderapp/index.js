import "@babel/polyfill";
import ExchangeFactory from './binanceFactory';
import {MockDataRecorder, UTCDate} from 'a1xr-utilities';

var path = require('path');
global.appRoot = path.resolve(__dirname);
global.appRoot = appRoot.replace('/recorderapp', '');

var exchange = ExchangeFactory();

let record = function(symbol){
    recordData(symbol);
}

let recordData = function(symbol){
    var limit = 1000;

    var curDate = UTCDate.getUTCDate();
    var startDate = UTCDate.getUTCDate();
    startDate.getUTCDate(-1*limit);

    var endTime = curDate.getTime();
    var startTime = startDate.getTime();

    var interval = '1d';

    exchange.candles({
        symbol : symbol,
        interval : interval,
        limit : limit
    }).then(data =>{
        console.log(symbol);
        MockDataRecorder.writeFile('timeseries', [symbol, interval, endTime].join('-') + '.json', data);
    });
}

var symbols = [];
var i = 0;
var timer;

module.exports = function () {
    exchange.exchangeInfo().then(async data =>{
        symbols = data.symbols;
        var offset = 1000;

        timer = setInterval(function(){
            if(i < symbols.length){
                record(symbols[i].symbol);
                i++;
            }else{
                clearInterval(timer);
            }
        }, 1000);
});
    
};
