import Binance from './binance-api-node';
import BinanceMock from './models/binanceMock';

module.exports = function(apiKey, apiSecret, mock = false){
    if(mock){
        return BinanceMock;
    }else{
        return Binance({
            apiKey: apiKey,
            apiSecret: apiSecret
        });
    }
};