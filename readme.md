# Binance Bot

## High Level Business Requirements
```

Start with a symbol set in a file

Stop Loss Notes:
- Is there a way to determine if a stop loss has been triggered for symbol?
- If stop loss has been triggered then remove symbol.
    - Maybe consider 2 algos in the future; bull and bear algos. Switch based on stop loss.
    ```
        Stop Loss Triggered then limit order at 5% lower then bid price
        Stop Loss Triggered again remove symbol from trading
    ```

Pseudo Flow:

- Set Buy Orders for 1-2% lower then Market
- Record Price in DB
- Poll when buy order is executed
- If buy order is executed set stop loss for 2-3% lower then Market
- Set sell order 3% higher then Market
- If stop loss executed set buy order for 2-3% lower then Market
    - Check stop loss threshold. If greater or equal remove symbol from trading
- Rinse and Repeat


Strategies:
- Try Trading based on pure MA indicators 7, 25, 29 over a 5m interval, use Channel for Trend
- ChannelBased Trading with indicator trigger
- ModifiedChannel


