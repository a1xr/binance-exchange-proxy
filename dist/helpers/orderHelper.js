"use strict";

var _decimal = require("decimal.js");

var _a1xrUtilities = require("a1xr-utilities");

var _v = _interopRequireDefault(require("uuid/v4"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var OrderHelper =
/*#__PURE__*/
function () {
  /*
  *   exchange, logger, orderIdPrefix, runlive
  */
  function OrderHelper(model) {
    _classCallCheck(this, OrderHelper);

    this.model = model;
  }

  _createClass(OrderHelper, [{
    key: "generateOrderId",
    value: function generateOrderId() {
      var id = (0, _v["default"])();
      return this.model.orderIdPrefix + '-' + id.substring(this.model.orderIdPrefix.length + 1);
    }
  }, {
    key: "generateStopLossOrderId",
    value: function generateStopLossOrderId() {
      var id = (0, _v["default"])();
      return 'sl-' + id.substring(3);
    }
  }, {
    key: "hasOpenOrder",
    value: function hasOpenOrder(openOrders) {
      var _this = this;

      return openOrders.find(function (order) {
        return order.clientOrderId.indexOf(_this.model.orderIdPrefix + '-') >= 0;
      }) != undefined;
    }
  }, {
    key: "hasStaleBuyOrder",
    value: function hasStaleBuyOrder(openOrders, serverTime, minuteThreshold) {
      var _this2 = this;

      var orders = openOrders.filter(function (order) {
        return order.clientOrderId.indexOf(_this2.model.orderIdPrefix) >= 0 && order.side == _a1xrUtilities.OrderEnums.SIDE_BUY;
      });

      if (orders && orders.length && new Date(serverTime).getUTCMinutes() - new Date(orders[0].time).getUTCMinutes() > minuteThreshold) {
        return orders[0];
      }

      return null;
    }
    /*
        args: symbol, orderValue, qty, qtyPrecision, currentPrice, stopLossPct, tradeFee, priceModifierPct
    */

  }, {
    key: "placeSellOrder",
    value: function placeSellOrder(args) {
      var _this3 = this;

      var symbol = args.symbol;
      var priceModifierPct = args.priceModifierPct || 0;
      var tradeFee = new _decimal.Decimal(args.tradeFee);
      var currentAskPrice = new _decimal.Decimal(args.currentPrice).plus(new _decimal.Decimal(args.currentPrice).times(tradeFee)).plus(new _decimal.Decimal(args.currentPrice).times(priceModifierPct));
      var lotSize = new _decimal.Decimal(Number(args.qty).toFixed(args.qtyPrecision));
      var orderRevenue = currentAskPrice.times(lotSize);
      var orderFee = orderRevenue.times(tradeFee);
      var orderArgs = {
        symbol: symbol,
        side: _a1xrUtilities.OrderEnums.SIDE_SELL,
        type: _a1xrUtilities.OrderEnums.LIMIT,
        quantity: lotSize,
        price: currentAskPrice.toFixed(args.pricePrecision),
        newClientOrderId: this.generateOrderId()
      };
      var orderNote = symbol + " sell price " + currentAskPrice + " qty " + lotSize;
      var order = this.model.runlive ? this.model.exchange.order(orderArgs) : this.model.exchange.orderTest(orderArgs);
      console.log(orderNote);
      return order.then(function (result) {
        if (result.clientOrderId) {
          _this3.model.tradeTracker.addTrade({
            orderid: result.orderId,
            client_orderid: result.clientOrderId,
            symbol: result.symbol,
            transact_time: result.transact_time,
            symbol_price: result.price,
            order_fee: orderFee,
            lot_size: result.origQty,
            order_type: _a1xrUtilities.OrderEnums.SIDE_SELL,
            raw_response: JSON.stringify(result)
          }).then();
        }
      })["catch"](function (err) {
        console.log(err);

        _this3.model.logger.log(orderArgs, err, orderNote, _this3.model.logger.constants.ERROR).then();
      });
    }
  }, {
    key: "placeImmediateSellOrder",
    value: function placeImmediateSellOrder(args) {
      var _this4 = this;

      var symbol = args.symbol;
      var priceModifierPct = args.priceModifierPct || 0;
      var tradeFee = new _decimal.Decimal(args.tradeFee);
      var currentAskPrice = new _decimal.Decimal(args.currentPrice);
      var lotSize = new _decimal.Decimal(Number(args.qty).toFixed(args.qtyPrecision));
      var orderRevenue = currentAskPrice.times(lotSize);
      var orderFee = orderRevenue.times(tradeFee);
      var orderArgs = {
        symbol: symbol,
        side: _a1xrUtilities.OrderEnums.SIDE_SELL,
        type: _a1xrUtilities.OrderEnums.LIMIT,
        quantity: lotSize,
        price: currentAskPrice.toFixed(args.pricePrecision),
        newClientOrderId: this.generateStopLossOrderId()
      };
      var orderNote = symbol + " immediate sell price " + currentAskPrice + " qty " + lotSize;
      var order = this.model.runlive ? this.model.exchange.order(orderArgs) : this.model.exchange.orderTest(orderArgs);
      console.log(orderNote);
      return order.then(function (result) {
        if (result.clientOrderId) {
          _this4.model.tradeTracker.addTrade({
            orderid: result.orderId,
            client_orderid: result.clientOrderId,
            symbol: result.symbol,
            transact_time: result.transact_time,
            symbol_price: result.price,
            order_fee: orderFee,
            lot_size: result.origQty,
            order_type: _a1xrUtilities.OrderEnums.SIDE_SELL,
            raw_response: JSON.stringify(result)
          }).then();
        }
      })["catch"](function (err) {
        _this4.model.logger.log(orderArgs, err, orderNote, _this4.model.logger.constants.ERROR).then();
      });
    }
    /*
        args: symbol, orderValue, qty, qtyPrecision, currentPrice, stopLossPct, tradeFee, priceModifierPct
    */

  }, {
    key: "placeBuyOrder",
    value: function placeBuyOrder(args) {
      var _this5 = this;

      var symbol = args.symbol;
      var priceModifierPct = args.priceModifierPct || 0;
      var orderValue = new _decimal.Decimal(args.orderValue);
      var tradeFee = new _decimal.Decimal(args.tradeFee);
      var currentBidPrice = new _decimal.Decimal(args.currentPrice).minus(new _decimal.Decimal(args.currentPrice).times(tradeFee)).minus(new _decimal.Decimal(args.currentPrice).times(priceModifierPct));
      var stopLoss = currentBidPrice.minus(new _decimal.Decimal(args.stopLossPct).times(currentBidPrice));
      var lotSize = orderValue.dividedBy(currentBidPrice).toFixed(args.qtyPrecision);
      var orderFee = orderValue.times(tradeFee); //place buy based on risk

      var pip = PipCalculator.getUsdPipRiskReward(orderValue, lotSize, currentBidPrice.decimalPlaces());
      var pipsl = PipCalculator.getUsdPipRiskRewardWithStopLoss(stopLoss, orderValue, lotSize, currentBidPrice.decimalPlaces());
      var price = currentBidPrice.toFixed(args.pricePrecision);
      var orderArgs = {
        symbol: symbol,
        side: _a1xrUtilities.OrderEnums.SIDE_BUY,
        type: _a1xrUtilities.OrderEnums.LIMIT,
        quantity: lotSize,
        price: price,
        newClientOrderId: this.generateOrderId()
      };
      var orderNote = symbol + " buy price " + price + " qty " + lotSize;
      var order = this.model.runlive ? this.model.exchange.order(orderArgs) : this.model.exchange.orderTest(orderArgs);
      console.log(orderNote);
      return order.then(function (result) {
        if (result.clientOrderId) {
          _this5.model.tradeTracker.addTrade({
            orderid: result.orderId,
            client_orderid: result.clientOrderId,
            symbol: result.symbol,
            transact_time: result.transact_time,
            symbol_price: result.price,
            order_fee: orderFee,
            lot_size: result.origQty,
            pip: pip,
            pipsl: pipsl,
            order_type: _a1xrUtilities.OrderEnums.SIDE_BUY,
            raw_response: JSON.stringify(result)
          }).then();
        }
      })["catch"](function (err) {
        console.log(err);

        _this5.model.logger.log(orderArgs, err, orderNote, _this5.model.logger.constants.ERROR).then();
      });
    }
    /*
        args: symbol, qty, qtyPrecision, currentPrice, stopLossPct, tradeFee
    */

  }, {
    key: "setStopLoss",
    value: function setStopLoss(args) {
      var _this6 = this;

      var currentPrice = new _decimal.Decimal(args.price);
      var stopLossPrice = currentPrice.minus(new _decimal.Decimal(args.stopLossPct).times(currentPrice));
      var qty = new _decimal.Decimal(args.qty).toFixed(assetData.seedData.qtyPrecision);
      var tradeFee = new _decimal.Decimal(args.tradeFee);
      var orderValue = currentPrice.times(qty);
      var orderFee = orderValue.times(tradeFee);
      var orderArgs = {
        symbol: symbol,
        type: _a1xrUtilities.OrderEnums.STOP_LOSS,
        quantity: qty,
        price: stopLossPrice.toFixed(args.pricePrecision),
        newClientOrderId: this.generateStopLossOrderId()
      };
      var orderNote = symbol + " stoploss price " + stopLossPrice + " qty " + qty; //console.log(this.model.runlive);

      var order = this.model.runlive ? this.model.exchange.order(orderArgs) : this.model.exchange.orderTest(orderArgs);
      return order.then(function (result) {
        _this6.model.tradeTracker.addTrade({
          orderid: result.orderId,
          client_orderid: result.clientOrderId,
          symbol: result.symbol,
          transact_time: result.transact_time,
          symbol_price: result.price,
          order_fee: orderFee,
          lot_size: result.origQty,
          order_type: _a1xrUtilities.OrderEnums.STOP_LOSS,
          raw_response: result.toJSON()
        }).then();
      })["catch"](function (err) {
        _this6.model.logger.log(orderArgs, err, orderNote, _this6.model.logger.constants.ERROR).then();
      });
    }
  }, {
    key: "cancelOrder",
    value: function cancelOrder(order) {
      var _this7 = this;

      var orderArgs = {
        symbol: order.symbol,
        orderId: order.orderId
      };
      var orderNote = order.symbol + " cancel_order price " + order.orderId;
      console.log(orderNote);
      return this.model.exchange.cancelOrder(orderArgs).then(function (result) {
        _this7.model.tradeTracker.removeTrade({
          orderId: result.orderId
        }).then();
      })["catch"](function (err) {
        _this7.model.logger.log(orderArgs, err, orderNote, _this7.model.logger.constants.ERROR).then();
      });
      ;
    }
  }]);

  return OrderHelper;
}();

module.exports = OrderHelper;