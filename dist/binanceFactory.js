"use strict";

var _binanceApiNode = _interopRequireDefault(require("./binance-api-node"));

var _binanceMock = _interopRequireDefault(require("./models/binanceMock"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

module.exports = function (apiKey, apiSecret) {
  var mock = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

  if (mock) {
    return _binanceMock["default"];
  } else {
    return (0, _binanceApiNode["default"])({
      apiKey: apiKey,
      apiSecret: apiSecret
    });
  }
};